﻿#include <iostream>
#include <string>


const int N = 100;

void print_number(std::string even, int n)
{
	if (even == "even") 
	{
		for (int i = 2; i <= n; ++i)
		{
			if (i % 2 == 0)
			{
				std::cout << i << " ";
			}
		}
	}	
	else	
	{
		for (int i = 0; i <= n; ++i) {
			if (i % 2 != 0)
			{
				std::cout << i << " ";
			}
		}
	}
	std::cout << std::endl;
}

int main()
{
	print_number("even", N);
	print_number("not even", N);

	return 0;
}